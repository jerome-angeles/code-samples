import pandas as pd
import numpy as np
import re

housedf = pd.read_csv('City_Zhvi_AllHomes.csv').set_index(['State', 'RegionName']).drop(columns=['RegionID', 'Metro', 'CountyName', 'SizeRank'])
states = {'OH': 'Ohio', 'KY': 'Kentucky', 'AS': 'American Samoa', 'NV': 'Nevada', 'WY': 'Wyoming', 'NA': 'National', 'AL': 'Alabama', 'MD': 'Maryland', 'AK': 'Alaska', 'UT': 'Utah', 'OR': 'Oregon', 'MT': 'Montana', 'IL': 'Illinois', 'TN': 'Tennessee', 'DC': 'District of Columbia', 'VT': 'Vermont', 'ID': 'Idaho', 'AR': 'Arkansas', 'ME': 'Maine', 'WA': 'Washington', 'HI': 'Hawaii', 'WI': 'Wisconsin', 'MI': 'Michigan', 'IN': 'Indiana', 'NJ': 'New Jersey', 'AZ': 'Arizona', 'GU': 'Guam', 'MS': 'Mississippi', 'PR': 'Puerto Rico', 'NC': 'North Carolina', 'TX': 'Texas', 'SD': 'South Dakota', 'MP': 'Northern Mariana Islands', 'IA': 'Iowa', 'MO': 'Missouri', 'CT': 'Connecticut', 'WV': 'West Virginia', 'SC': 'South Carolina', 'LA': 'Louisiana', 'KS': 'Kansas', 'NY': 'New York', 'NE': 'Nebraska', 'OK': 'Oklahoma', 'FL': 'Florida', 'CA': 'California', 'CO': 'Colorado', 'PA': 'Pennsylvania', 'DE': 'Delaware', 'NM': 'New Mexico', 'RI': 'Rhode Island', 'MN': 'Minnesota', 'VI': 'Virgin Islands', 'NH': 'New Hampshire', 'MA': 'Massachusetts', 'GA': 'Georgia', 'ND': 'North Dakota', 'VA': 'Virginia'}
econdf = pd.read_excel('gdplev.xls', usecols='E:G', skiprows=219, header=0).dropna()
econdf.columns=['Quarter', 'GDP in Current Dollars', 'GDP in 2009 Dollars']
econdf.set_index('Quarter', inplace=True)
econdf = econdf.join(econdf.pct_change(), how='left', rsuffix=' (change)')

print(housedf)
def get_list_of_university_towns(): #TODO Make this pandorable https://stackoverflow.com/questions/41457322/pandas-rearranging-a-data-frame/41458629#41458629
    '''Returns a DataFrame of towns and the states they are in from the 
    university_towns.txt list. The format of the DataFrame should be:
    DataFrame( [ ["Michigan", "Ann Arbor"], ["Michigan", "Yipsilanti"] ], 
    columns=["State", "RegionName"]  )
    
    The following cleaning needs to be done:

    1. For "State", removing characters from "[" to the end.
    2. For "RegionName", when applicable, removing every character from " (" to the end.
    3. Depending on how you read the data, you may need to remove newline character '\n'. '''

    univ_file = open('university_towns.txt', 'r')
    current_state = None
    townsdf = pd.DataFrame(columns=['State', 'RegionName'])
    c = 0
    for line in univ_file:
    	if '[edit]' in line:
    		current_state = line.replace('[edit]\n', '')
    	else:
    		townsdf.loc[c] = {'State': current_state, 'RegionName': re.sub('\s*\(.*\n', '', line)}
    		c = c+1
    
    return townsdf

def get_recession_start():
    '''Returns the year and quarter of the recession start time as a 
    string value in a format such as 2005q3'''
    #get recession start
    criteria1 = econdf['GDP in 2009 Dollars (change)'] < 0
    criteria2 = econdf['GDP in 2009 Dollars (change)'].shift(-1) < 0
    criteria3 = econdf['GDP in 2009 Dollars (change)'].shift(-2) < 0
    return econdf.where(criteria1 & criteria2 & criteria3).dropna().index[0]

def get_recession_end():
    #get recession start
    criteria1 = econdf['GDP in 2009 Dollars (change)'] < 0
    criteria2 = econdf['GDP in 2009 Dollars (change)'].shift(1) < 0
    criteria3 = econdf['GDP in 2009 Dollars (change)'].shift(2) < 0
    return econdf.where(criteria1 & criteria2 & criteria3).dropna().index[-1]

def get_recession_bottom():
	start = get_recession_start()
	end = get_recession_end()
	return econdf.loc[start: end]['GDP in 2009 Dollars (change)'].idxmin()

def convert_housing_data_to_quarters():
    '''Converts the housing data to quarters and returns it as mean 
    values in a dataframe. This dataframe should be a dataframe with
    columns for 2000q1 through 2016q3, and should have a multi-index
    in the shape of ["State","RegionName"].
    
    Note: Quarters are defined in the assignment description, they are
    not arbitrary three month periods.
    
    The resulting dataframe should have 67 columns, and 10,730 rows.
    '''
    timeperiod_category = pd.date_range('2000/1/1', periods=67, freq='Q')
    #housedf.groupby(pd.PeriodIndex(housedf.columns, freq='Q'), axis=1)
    
    return housedf.groupby(pd.PeriodIndex(housedf.columns, freq='Q'), axis=1)

print(get_list_of_university_towns())
print(get_recession_start())
print(get_recession_end())
print(get_recession_bottom())
print(convert_housing_data_to_quarters())
#print(pd.to_datetime(econdf.index))
#print(pd.date_range('2000/1/1', periods=67, freq='Q'))