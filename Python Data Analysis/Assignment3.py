import pandas as pd
import numpy as np

energy = pd.read_excel('Energy Indicators.xls', skiprows=17, header=0, nrows=226, usecols="C:F", na_values=["...", 0])
GDP = pd.read_csv('world_bank.csv', skiprows=2, header=2)
ScimEn = pd.read_excel('scimagojr-3.xlsx', header=0)

#Replaces columns in energy DataFrame
energy.columns = 	['Country', 
					'Energy Supply', 
					'Energy Supply per Capita', 
					'% Renewable']

#Rename column in GDP to match the others
GDP.rename({'Country Name': 'Country'}, axis=1, inplace=True)

def answer_one():
	#Columns to be renamed and kept
	columns_to_be_kept =	['Rank', 'Documents', 
							'Citable documents', 'Citations', 
							'Self-citations', 'Citations per document', 
							'H index', 'Energy Supply', 
							'Energy Supply per Capita', '% Renewable', 
							'2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015']

	energy_country_rename = 	{"Republic of Korea": "South Korea",
								"United States of America": "United States",
								"United Kingdom of Great Britain and Northern Ireland": "United Kingdom",
								"China, Hong Kong Special Administrative Region": "Hong Kong"}

	gdp_country_rename = {"Korea, Rep.": "South Korea", 
						"Iran, Islamic Rep.": "Iran",
						"Hong Kong SAR, China": "Hong Kong"}

	#Replace Countries with parenthesis or numbers in them
	energy['Country'].replace(to_replace='\s*\(.*\)', value='', regex=True, inplace=True)
	energy['Country'].replace(to_replace='\d*', value='', regex=True, inplace=True)

	#Rename countries in energy
	for index, value in energy['Country'].items():
		if value in energy_country_rename.keys():
			energy.loc[index, 'Country'] = energy_country_rename[value]
	#Renamce countries in gdp
	for index, value in GDP['Country'].items():
		if value in gdp_country_rename.keys():
			GDP.loc[index, 'Country'] = gdp_country_rename[value]

	#only get top 15 countries in rank
	cat1 = ScimEn['Rank'] >= 0
	cat2 = ScimEn['Rank'] <= 15
	ranked = ScimEn.where(cat1 & cat2).dropna()

	#merge
	merged = pd.merge(ranked, energy, how='inner', on='Country')
	merged = pd.merge(merged, GDP, how='inner', on='Country')

	return merged.set_index(['Country'])[columns_to_be_kept]

a1df = answer_one()

def answer_two():
	#merge
	merged = pd.merge(ScimEn, energy, how='outer', on='Country')
	merged = pd.merge(merged, GDP, how='outer', on='Country')
	return merged.index.size - a1df.index.size

def mask_average(val):
	nanmask = np.isnan(val)
	x = np.ma.array(val.values, mask=nanmask)
	return np.ma.average(x)

def answer_three():
	op = a1df.T[-10:].T
	return (pd.Series(op.aggregate(mask_average, axis=1), name='avgGDP')
			.sort_values(ascending=False))

def answer_four():
	op = a1df.T[-10:].T.iloc[6] #Get the 6th ranked
	return op.max() - op.min()

def answer_five():
	return a1df['Energy Supply per Capita'].mean()

def answer_six():
	id = a1df['% Renewable'].idxmax()
	return (id, a1df.at[id, '% Renewable'])

def answer_seven():
	tempdf = a1df.copy()
	tempdf['Citations ratio'] = tempdf['Citations'] / tempdf['Self-citations']
	biggest_country = tempdf['Citations ratio'].idxmax()
	return (biggest_country, tempdf.at[biggest_country, 'Citations ratio'])

def answer_eight():
	a1df['PopEst'] = (a1df['Energy Supply'] / a1df['Energy Supply per Capita']) * 1000000
	return (a1df['PopEst']
			.sort_values(ascending=False)
			.index[2])

def answer_nine():
	a1df['Citable docs per Capita'] = a1df['Citable documents'] / a1df['PopEst']
	return a1df['Citable docs per Capita'].corr(a1df['Energy Supply per Capita'])

def median_test(val, mean):
	if(val > mean):
		return 1
	else:
		return 0

def answer_ten():
	a1df['HighRenew'] = (a1df['% Renewable']
							.apply(median_test, mean=a1df['% Renewable'].mean()))
	return a1df['HighRenew']

ContinentDict  = {'China':'Asia', 
                  'United States':'North America', 
                  'Japan':'Asia', 
                  'United Kingdom':'Europe', 
                  'Russian Federation':'Europe', 
                  'Canada':'North America', 
                  'Germany':'Europe', 
                  'India':'Asia',
                  'France':'Europe', 
                  'South Korea':'Asia', 
                  'Italy':'Europe', 
                  'Spain':'Europe', 
                  'Iran':'Asia',
                  'Australia':'Australia', 
                  'Brazil':'South America'}

def answer_eleven():
	a1df['Continent'] = pd.Series(ContinentDict, name='Continent')
	return (a1df.groupby('Continent')
				.agg({'PopEst': [np.size, np.sum, np.mean, np.std]}))

def answer_twelve():
	return a1df.groupby(['Continent', pd.cut(a1df['% Renewable'], 5)]).size()

def answer_thirteen():
	return a1df['PopEst'].map(lambda x: "{:,}".format(x))

print("Answer One")
print(a1df)
print("Answer Two")
print(answer_two())
print("Answer Three")
print(answer_three())
print("Answer Four")
print(answer_four())
print("Answer Five")
print(answer_five())
print("Answer Six")
print(answer_six())
print("Answer Seven")
print(answer_seven())
print("Answer Eight")
print(answer_eight())
print("Answer Nine")
print(answer_nine())
print("Answer Ten")
print(answer_ten())
print("Answer Eleven")
print(answer_eleven())
print("Answer Twelve")
print(answer_twelve())
print("Answer Thirteen")
print(answer_thirteen())