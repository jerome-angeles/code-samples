# A comment, this is so you can read your programs later
# Anything after the # is ignored by python

print("I could have a code like this.") # and comment after is ignored

# You can also comment to "disable" or comment out code
# print("This one won't run")

print("This will run")