states = {
	'Oregon': 'OR',
	'Florida': 'FL',
	'California': 'CA',
	'New York': 'NY',
	'Michigan': 'MI'
}

cities = {
	'CA': 'San Francisco',
	'MI': 'Detroit',
	'FL': 'Jacksonville'
}

#add some more cities
cities['NY'] = 'New York'
cities['OR'] = 'Portland'

print('-'*10)

print("NY State has:", cities['NY'])
print("OR State has:", cities['OR'])

#print some states
print('-'*10)
print("Michigan's abbreviation is:", states['Michigan'])
print("Florida's abbreviation is:", states['Florida'])

#do it by using the state then cities dict
print('-'*10)
print("Michigan has:", cities[states['Michigan']])
print("Florida has:", cities[states['Florida']])

#Print every state abbreviation
print('-'*10)
for state, abbrev in list(states.items()):
	print(f"{state} is abbreviated as {abbrev}")

#Now do it at the same time
print('-'*10)
for state, abbrev in list(states.items()):
	print(f"{state} state is abbreviated {abbrev}")
	print(f"and has city {cities[abbrev]}")

print('-'*10)
# Safely get a abbreviation by state might not be there
state = states.get('Texas')

if not state:
	print("Sorry, no Texas")

# Get a city with a default value
city = cities.get('TX', 'Does Not Exist')
print(f"The city for the state 'TX' is: {city}")