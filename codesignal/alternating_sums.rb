def alternatingSums(a)
    #@type [Array]
    b = a
    x = 0
    y = 0
    b.each_with_index do
        |c, index|
        x += c if index % 2 == 0
        y += c if index % 2 == 1
    end
    return [x,y]
end

x = alternatingSums([50, 60, 60, 45, 70])
x