def factorialTrailingZeros(n)
    return ((1..n).inject(1) {|r, i| r*=i}).to_s.match(/0*$/)[0].length
end