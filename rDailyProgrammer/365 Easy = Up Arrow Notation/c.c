/*
[2018-06-18] Challenge #364 [Easy] Up Arrow Notation
Description - Pure C, no arguments
Author - Jerome Angeles
Date 7/13/2018
Link - https://www.reddit.com/r/dailyprogrammer/comments/8xbxi9/20180709_challenge_365_easy_uparrow_notation/
*/

#include <stdio.h>
#include <math.h>
#include <string.h>

long up(long x, long y, int arrow_count)
{
	if(arrow_count == 1)
		return pow(x, y);
	else if(arrow_count >= 1 && y == 0)
		return 1;
	else if (x == -1 || x == 1)
		return x; //Just returns it, it will not change anyways
	else
	{
		long y2 = up(x, y - 1, arrow_count);
		return up(x, y2, arrow_count - 1);
	}
}

int only_arrows(char *op)
{
	for (int i = 0; i < strlen(op); ++i)
	{
		if(op[i] != 24)
			return 0;
	}
	return 1;
}

int main(void){
	char * input;
	int x = 0, y = 0, arrows = 0;
	long result = 0;

	while(1)
	{
		fgets(input, 25, stdin);
		if(sscanf(input, "%d %s %d", &x, input, &y) == 3 && 
			only_arrows(input))
		{
			arrows = strlen(input);
			result = up(x,y,arrows);
			printf("=> %ld\n", result);
		}
		else
		{
			printf("%s\n", "Try again!");
		}
		fflush(stdin);
	}
	return 0;
}