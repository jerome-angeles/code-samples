/*
[2018-06-18] Challenge #364 [Easy] Create a Dice Roller
Description - Pure C, no arguments, no pointers version (I have not been there yet in my latest review)
Author - Jerome Angeles
Date 7/2/2018
Link - https://www.reddit.com/r/dailyprogrammer/comments/8s0cy1/20180618_challenge_364_easy_create_a_dice_roller/
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void){
	
	//Initialize randomizer seed using system time
	srand(time(NULL));

	//Variable initialization
	int n_dice = 0;
	int n_sides = 0;
	int sum = 0;
	int rolls[100];

	/*
		3d6 = 3 dice 6 sided
		4d12 = 4 dice 12 sided etc...
		The first number, the number of dice to roll, can be any integer between 1 and 100, inclusive.
		The second number, the number of sides of the dice, can be any integer between 2 and 100, inclusive.
	*/
	puts("Dice Roller\nEnter Dice to Roll\nFormat: \"#d#\" where # is a positive integer");

	if(scanf("%dd%d", &n_dice, &n_sides)){
		if((n_dice >= 1 && n_dice <= 100) && (n_sides >= 2 && n_sides <= 100)) { //Check bounds
			//roll
			for(int c = 0; c < n_dice; c++){
				int roll = 1 + rand() % n_sides;
				rolls[c] = roll; //For saving. To print all later
				sum += roll;
			}
		}
		else { //If it failed
			puts("Invalid Input, restart the program");
			exit(EXIT_FAILURE);
		}
	}
	else { //lol
		puts("Invalid Input, restart the program");
		exit(EXIT_FAILURE);
	}
	printf("%4d: ", sum);
	for(int c = 0; c < n_dice; c++){
		printf("%3d ", rolls[c]);
	}
	exit(EXIT_SUCCESS);
}