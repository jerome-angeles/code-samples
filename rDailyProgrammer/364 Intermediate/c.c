/*
[2018-06-18] Challenge #364 [Easy] Ducci Sequence
Description - Pure C, no arguments, no pointers version (I have not been there yet in my latest review)
			- Uses recursion (which causes stack overflow bugs gomenasai T-T)
Author - Jerome Angeles
Date 7/3/2018
Link - https://www.reddit.com/r/dailyprogrammer/comments/8sjcl0/20180620_challenge_364_intermediate_the_ducci/
*/

#include <stdio.h>
#include <stdlib.h>

#define size int
#define bool int
#define true 1
#define false 0

//does the ducci
void ducci(int[], size, bool);
bool check_tuples_if_valid(int[], size);
void append_tuple_to_checked(int[], size, int);
void print_tuple(int[], size);

//checked_tuples[ROW][COLUMN]
int checked_tuples[255][255];

int main(){
	int input = 0;
	int length = 0;
	int tuple[255]; //Will work as long as the length of the tuple is also passed when ducci is called
	printf("%s\n", "Enter Ducci Elements (- number to stop)");
	while (true) {
		if(length > 200){
			printf("%s\n", "Too many elements, time to ducci");
			break;
		}
		printf("%7s, %-4d", "Element", length);
		if(scanf("%d", &input)){
			//If input is valid
			if(input >= 0){
				tuple[length] = input;
				length++;
			}
			else {
				break;
			}
		}
		else {
			//TODO: CLEAR STDIN before continuing
			continue;
		}
	}
	print_tuple(tuple, length);
	ducci(tuple, length, true);
	exit(EXIT_SUCCESS);
}

void ducci(int tuple[], size arr_length, bool c_start){
	/*
		Algorithm
		Make new array of size arr_length and fill it up with zeroes
		make element 'n' of the new array = tuple[n] - tuple[n+1] of passed tuple and the last element = tuple[last] - tuple[first]
		call function again until {0,0,0,0} is achieved
	*/
	static int step_counter = 1;
	if(c_start)
		step_counter = 1;
	if(step_counter > 200) {
		printf("%s\n", "Took too much steps on this one");
		return;
	}

	int new_arr[arr_length];
	for(int c = 0; c < arr_length; c++){
		if(c > arr_length)
			new_arr[c] = abs(tuple[c] - tuple[c+1]);
		else
			new_arr[c] = abs(tuple[arr_length] - tuple[0]);
	}

	print_tuple(new_arr, arr_length);
	step_counter++;

	//If all elements of the tuple is zero
	if(check_tuples_if_valid(new_arr, arr_length)) {
		printf("Steps %d\n", step_counter);
		return;
	}
	else {		
		append_tuple_to_checked(new_arr, arr_length, step_counter);
		ducci(new_arr, arr_length, false);
	}
}

void print_tuple(int tuple[], size arr_length){
	for(int c = 0; c < arr_length; c++){
		if(c==0){
			printf("{%d", tuple[c]);
		}
		else if (c == arr_length - 1){
			printf(",%d}\n", tuple[c]);	
		}
		else{
			printf(",%d", tuple[c]);	
		}
	}
}

void append_tuple_to_checked(int tuple[], size arr_length, int position){
	for(int c = 0; c < arr_length; c++){
		checked_tuples[position][c] = tuple[c];
	}
}

//Check if tuples is now equal to zero
bool check_tuples_if_valid(int tuple[], size arr_length){
	/*
	This one checks for 2 situations
	1. If the tuple becomes all 0
	2. If it made a repeat tuple

	The first one is easy to do just check if all of it in zero, if one is not, then it is not.
	Second one goes like this
		On all of the elements on the recorded_tuples (checked_tuples) do
			compare current tuple with recorded ones. If one field is different then it is not equal.
	*/
	bool result = true;
	for(int c = 0; c < arr_length; c++){
		if(tuple[c] != 0)
			result = false;
	}
	for(int c_on_checked = 0; c_on_checked < 255; c_on_checked++){
		bool all_equal_flag = true;
		for(int c = 0; c < arr_length; c++){
			if(tuple[c] != checked_tuples[c_on_checked][c]){
				all_equal_flag = false;
				break;
			}
		}
		if(all_equal_flag == true){
			return true;
		}
	}
	return result;
}