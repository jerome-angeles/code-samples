/**
 * [2019-02-15] Challenge #375 [Hard] Graph of Thrones
 * Link: https://www.reddit.com/r/dailyprogrammer/comments/aqwvxo/20190215_challenge_375_hard_graph_of_thrones/
 * 
 * Solution by: Jerome Angeles
 */

const Graph = require('graphlib').Graph;
const fs = require('fs');
const readline = require('readline');
const _ = require('lodash');

//Get the input from input file
let rl = readline.createInterface(
    fs.createReadStream('input.txt', {encoding: "UTF-8"})
);

//Create new graph object
let g = new Graph()

/**
 * Assigns a particular symbol on 2 nodes of the graph, if nodes does not yet exists
 * the function will create the node
 * 
 * @param {String} node1 First node
 * @param {String} node2 Second node
 * @param {String} symbol Symbol to be associated with the first and second node
 */
function splitAndAssign(node1, node2, symbol){
    if(!_.has(g.nodes(), node1)){
        g.setNode(node1)
    }
    if(!_.has(g.nodes(), node2)){
        g.setNode(node2)
    }
    //Assigns edge where node1 and node2 is arrange in alphabetical order
    if(node1 < node2){
        g.setEdge(node1, node2, symbol);
    }
    else{
        g.setEdge(node2, node1, symbol);
    }
}

/**
 * Gets the symbol of a particular edge
 * 
 * @param {String} node1 first node
 * @param {String} node2 second node
 */
function getSymbol(node1, node2){
    //Gets the symbol where parameters are assigned in alphabetical order
    if(node1 < node2){
        return g.edge(node1, node2);
    }
    else{
        return g.edge(node2, node1);
    }
}

rl.on('line', (input) => {
    if(input.includes("++")){
        let nodes = _.split(input, '++');
        splitAndAssign(_.trim(nodes[0]), _.trim(nodes[1]), '++');
    }
    else if(input.includes("--")){
        let nodes = _.split(input, '--');
        splitAndAssign(_.trim(nodes[0]), _.trim(nodes[1]), '--');
    }
})

rl.on('close', () => {
    //check every node against each other
    let balance = true;
    let nodeCount = g.nodes().length;
    /**
     * To check if graph has structural integrity
     * For every set of 3 nodes check if the relationship between are all
     * positive, or 2 negative and 1 positive.
     * If those are true between all nodes and edges in a graph then the
     * network is stable
     */
    for (let c = 0; c < nodeCount - 2; c++) {
        for (let c2 = c + 1; c2 < nodeCount - 1; c2++) {
            for (let c3 = c + 2; c3 < nodeCount; c3++) {
                let positives = 0;
                const   firstNode = g.nodes()[c],
                        secondNode = g.nodes()[c2],
                        thirdNode = g.nodes()[c3];
                if(getSymbol(firstNode, secondNode) == '++'){
                    positives++;
                }
                if(getSymbol(secondNode, thirdNode) == '++'){
                    positives++;
                }
                if(getSymbol(thirdNode, firstNode) == '++'){
                    positives++;
                }
                balance = balance && (positives == 1 || positives == 3) 
            }
        }
    }
    if(balance){
        console.log("balanced");
    }
    else{
        console.log("unbalanced")
    }
})