/*
[2018-06-18] Challenge #365 [Intermediate] Sales Commissions (Not completed yet)
Description - Pure C, no arguments
Author - Jerome Angeles
Date 7/13/2018
Link - https://www.reddit.com/r/dailyprogrammer/comments/8xzwl6/20180711_challenge_365_intermediate_sales/
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define COMMISION 0.062
#define MAX_PEOPLE 32
#define MAX_PRODUCTS 32
#define MAX_LINE 1024

#define bool int
#define true 1
#define false 0

int main(int argc, char* argv[])
{
	FILE *input;
	char *current_name_input;
	char *names[MAX_PEOPLE];
	int revenue_matrix[MAX_PEOPLE][MAX_PRODUCTS];
	int expenses_matrix[MAX_PEOPLE][MAX_PRODUCTS];
	char *line = malloc(MAX_LINE + 1);
	if(argc != 2)
	{
		puts("No file arguments, or have too many arguments. Try again!");
		exit(EXIT_FAILURE);
	}

	input = fopen(argv[1], "r+");
	if(input == NULL)
	{
		puts("No file found!");
		exit(EXIT_FAILURE);
	}
	//Search for revenues keyword
	while(true){
		fgets(line, MAX_LINE, input);
		if(strstr(line, "Revenue"))
		{
			//skip another line
			fgets(line, MAX_LINE, input);
			break;
		}
	}

	//Save their names
	current_name_input = strtok(line, " ");
	for (int i = 0; current_name_input != NULL; ++i)
	{
		names[i] = current_name_input;
		strtok(line, " ");
	}

	exit(EXIT_FAILURE);
}