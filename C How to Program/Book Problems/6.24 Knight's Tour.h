#ifndef H_KNIGHTSTOUR

#define SYMBOL H_KNIGHTSTOUR

#define LANDED '!'
#define bool int
#define true 1
#define false 0

#define COLUMNS 8
#define ROWS 8
#define ROW 0
#define COLUMN 1

#define VECTOR 2
#define P puts

#define MOVE_LIMIT 8

#define MEMORY_LIMIT 64
#define SEQUENCE_LIMIT 256
#define SEARCH_DEPTH_LIMIT 6

#define DEBUG 0

//Globals
//Put into global to be initialized automatically
int INITIAL_POSITION[VECTOR] = {0,0};

//Move Matrix, Use to calculate possible moves.
const int MOVE_MATRIX[MOVE_LIMIT][VECTOR] = {{2, -1}, {1, -2},
									{-1, -2}, {-2, -1},
									{-2, 1}, {-1, 2},
									{1, 2}, {2, 1}};

//board and heuristics moved as a global variable, just not to spaggetisize the code
char board[ROWS][COLUMNS];
int board_heuristics[ROWS][COLUMNS];

//Prototypes
void prompt(void); //Shows prompt
void moves_get(const int[VECTOR], int[MOVE_LIMIT][VECTOR], int *); //Calculate for the next possible moves of the player
void board_update(const int[VECTOR], const int[MOVE_LIMIT][VECTOR], const int); //Updates characters to be displayed on the board
void board_clean(); //Cleans the board to be ready again.
void board_print(); //Prints the board
void cell_print(const char); //Prints a single cell
void loop(int[VECTOR]); //Does the loop construct

bool check_lost(const int);
bool check_win(const int[VECTOR]);
int ask_input(const int);
void player_reposition(int[VECTOR], const int[VECTOR]);



//Prototypes for Knights tour with Heuristics
void loop2(int[VECTOR]); //Loop with Heuristics
void ai_pick(const int[VECTOR], const int[MOVE_LIMIT][VECTOR], const int[MEMORY_LIMIT][VECTOR]);
void moves_get_w_invalidated(const int[VECTOR], int[MOVE_LIMIT][VECTOR], int *, const int[MEMORY_LIMIT][VECTOR], const int);
bool valid(const int[MEMORY_LIMIT][VECTOR], const int, const int);
void readjust_heuristics_slow();
void initialize_heuristics(); //Initializises heuristics board
void print_board_and_heuristics(); //Prints the board and the heuristics side by side
void get_move_with_lowest_heuristics(const int[MOVE_LIMIT][VECTOR], const int, int[MOVE_LIMIT][VECTOR], int *);
void print_previous_moves(const int[MEMORY_LIMIT][VECTOR], const int);


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#endif