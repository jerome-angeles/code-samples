/*
Name: Sales Tax
Author: Jerome Angeles
Date: 7/2/18

No pointers and CLI arguments (yet), no ifs

Pseudocode

While has not exited
	Initialize Variables
		Initialize total_collections, sales, county and state taxes to 0;

	Get Inputs
		Get total amount collected and month

	Calculate Taxes
	Display Tax Information
	Get user input if he is going to run the program again

Note:
	Month not checked because it is not needed by the program
*/

#include <stdio.h>
#include <stdlib.h>
#define COUNTY_TAX_RATE 0.05
#define STATE_TAX_RATE 0.04

int valid_input_flag = 0;

void check_if_valid_flag(void);

int main(void) {
	char * month;
	int total_collected = 0;
	float sales = 0, county_tax = 0, state_tax = 0, total_taxes = 0;

	while(1){
		printf("%s", "Enter total amount collected (-1 to exit): ");
		valid_input_flag = scanf("%d", &total_collected);
		check_if_valid_flag();
		if(total_collected < 0) {
			printf("%s\n", "Ending program");
			exit(EXIT_SUCCESS);
		}
		printf("%s", "Enter name of the month: ");
		scanf("%s", &month);

		county_tax = (float) total_collected * COUNTY_TAX_RATE;
		state_tax = (float) total_collected * STATE_TAX_RATE;
		total_taxes = state_tax + county_tax;
		sales = (float) total_collected - total_taxes;

		printf("Total Collections: $ %.2f\n", (float) total_collected);
		printf("Sales: $ %.2f\n", sales);
		printf("County Sales Tax: $ %.2f\n", county_tax);
		printf("State Sales Tax: $ %.2f\n", state_tax);
		printf("Total Sales Tax Collected: $ %.2f\n\n\n", total_taxes);
	}
	exit(EXIT_SUCCESS);
}

void check_if_valid_flag(void){
	if(!valid_input_flag){
		printf("%s\n", "Error in input, exiting with status error");
		exit(EXIT_FAILURE);
	}
}
