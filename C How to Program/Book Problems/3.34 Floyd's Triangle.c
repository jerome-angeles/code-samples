/*
Name: Floyd's Triangle
Author: Jerome Angeles
Date: 7/3/18

Floyd’s Triangle is a right-angled triangular array of natural numbers. It is defined by filling rows with consecutive integers. Thus, row 1 will have the number 1, row 2 willhave the numbers 2 and 3, and so on. Write a program that draws a 10-line Floyd’s triangle. Anouter loop can control the number of lines to be printed and an inner loop can ensure that each rowcontains the correct number of integers.

No pointers and CLI arguments (yet).
*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int total = 1;
	int height = 0;

	printf("%s\n", "Enter triengle height: ");
	if(!scanf("%d", &height)){
		printf("%s\n", "Wrong input, restart program");
		exit(EXIT_FAILURE);
	}
	for(int ch = 1; ch <= height; ch++) 
	{
		for (int c = 0; c < ch; c++)
		{
			printf("%-4d", total++);
		}
		printf("%s", "\n");
	}
	exit(EXIT_SUCCESS);
}