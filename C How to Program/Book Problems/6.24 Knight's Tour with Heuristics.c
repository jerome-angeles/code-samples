/*
Name: Knight's Tour
Author: Jerome Angeles
Date: 7/6/2018 - 7/7/2018

Full pledged knight's tour with Heuristics, self-solving

TODO:
Jump bug (not game breaking, only happens when you lost anyway)
*/

#include "6.24 Knight's Tour.h"

int main(void)
{
	//Initialize RNG
	srand(time(NULL));
	//Initialize the board
	for (int row = 0; row < ROWS; ++row)
	{
		for(int column = 0; column < COLUMNS; ++column)
		{
			board[row][column] = ' ';
		}
	}
	readjust_heuristics_slow();
	prompt();
	int player_pos[VECTOR];
	player_pos[ROW] = INITIAL_POSITION[ROW] - 1;
	player_pos[COLUMN] = INITIAL_POSITION[COLUMN] - 1;
	loop2(player_pos);
	exit(EXIT_FAILURE);
}

//You need to rewrite this if you need it to search deeper. Like, develop an algorithm for this and publish it into some college and stuff
void loop2(int player_pos[VECTOR])
{
	int possible_moves_array[MOVE_LIMIT][VECTOR];
	int resulting_moves[MOVE_LIMIT][VECTOR];
	int possible_moves_count = -1;
	int resulting_move_count = -1;
	int move_sequence[MEMORY_LIMIT][VECTOR];
	//Sequence Number - Move Number - Positions
	int best_candidates[SEQUENCE_LIMIT][MEMORY_LIMIT][VECTOR];
	bool lost = false, won = false;
	int move_counter = 0;
	//Initialize position
	move_sequence[move_counter][ROW] = player_pos[ROW];
	move_sequence[move_counter++][COLUMN] = player_pos[COLUMN];
	//Might look weird, but puts a P on the initial position
	player_reposition(player_pos, player_pos);
	print_board_and_heuristics();
	while(!lost || !won)
	{
		moves_get(player_pos, possible_moves_array, &possible_moves_count);
		get_move_with_lowest_heuristics(possible_moves_array, possible_moves_count, resulting_moves, &resulting_move_count);
		if(resulting_move_count > 1) //If possible moves is more than one
		{
			int sequence_counter = 0;
			int possible_moves_array2[MOVE_LIMIT][VECTOR];
			int invalidated[MEMORY_LIMIT][VECTOR];
			int possible_moves_count2 = 0;
			int current_heuristical_value = 0;
			int resulting_moves2[MOVE_LIMIT][VECTOR];
			int pick = 0;
			//Loop through possible moves
			for (int i = 0; i < resulting_move_count; ++i)
			{
				int choice = -1;
				int lowest = 100;
				int pos_placeholder[VECTOR] = {possible_moves_array[i][ROW], possible_moves_array[i][COLUMN]};
				//invalidated[0][ROW] = possible_moves_array[i][ROW];
				//invalidated[0][COLUMN] = possible_moves_array[i][COLUMN];
				invalidated[0][ROW] = player_pos[ROW];
				invalidated[0][COLUMN] = player_pos[COLUMN];
				current_heuristical_value = board_heuristics[possible_moves_array[i][ROW]][possible_moves_array[i][COLUMN]];
				//moves_get_w_invalidated(board, player_pos, possible_moves_array2, &possible_moves_count2, invalidated, 1);
				moves_get_w_invalidated(pos_placeholder, possible_moves_array2, &possible_moves_count2, invalidated, 1);
				get_move_with_lowest_heuristics(possible_moves_array2, possible_moves_count2, resulting_moves2, &possible_moves_count2);
				if(possible_moves_count2 == 1) //If there is only one move with the lowest heuristics here
				{
					current_heuristical_value += board_heuristics[resulting_moves2[0][ROW]][resulting_moves2[0][COLUMN]];
					if(current_heuristical_value < lowest) //if lower, reset the sequence counter, 
					{
						lowest = current_heuristical_value;
						sequence_counter = 0;
						best_candidates[sequence_counter][0][ROW] = possible_moves_array[i][ROW];
						best_candidates[sequence_counter][0][COLUMN] = possible_moves_array[i][COLUMN];
						best_candidates[sequence_counter][1][ROW] = resulting_moves2[0][ROW];
						best_candidates[sequence_counter][1][COLUMN] = resulting_moves2[0][COLUMN];
						++sequence_counter;
					}
					else if(current_heuristical_value == lowest)
					{
						//Add it to the sequence
						best_candidates[sequence_counter][0][ROW] = possible_moves_array[i][ROW];
						best_candidates[sequence_counter][0][COLUMN] = possible_moves_array[i][COLUMN];
						best_candidates[sequence_counter][1][ROW] = resulting_moves2[0][ROW];
						best_candidates[sequence_counter][1][COLUMN] = resulting_moves2[0][COLUMN];
						++sequence_counter;
					}
				}
				else if(possible_moves_count2 >= 2) //If there is more than one
				{
					current_heuristical_value += board_heuristics[resulting_moves2[0][ROW]][resulting_moves2[0][COLUMN]]; //since the total is all the same, might as well use the first one
					if(current_heuristical_value < lowest) //if lower, reset the sequence counter, 
					{
						lowest = current_heuristical_value;
						sequence_counter = 0;
						for (int c = 0; c < possible_moves_count2; ++c)
						{
							best_candidates[sequence_counter][0][ROW] = possible_moves_array[i][ROW];
							best_candidates[sequence_counter][0][COLUMN] = possible_moves_array[i][COLUMN];
							best_candidates[sequence_counter][1][ROW] = resulting_moves2[c][ROW];
							best_candidates[sequence_counter][1][COLUMN] = resulting_moves2[c][COLUMN];
							++sequence_counter;
						}
					}
					else if(current_heuristical_value == lowest)
					{
						for (int c = 0; c < possible_moves_count2; ++c)
						{
							best_candidates[sequence_counter][0][ROW] = possible_moves_array[i][ROW];
							best_candidates[sequence_counter][0][COLUMN] = possible_moves_array[i][COLUMN];
							best_candidates[sequence_counter][1][ROW] = resulting_moves2[c][ROW];
							best_candidates[sequence_counter][1][COLUMN] = resulting_moves2[c][COLUMN];
							++sequence_counter;
						}
					}
				}
				else //If there is none
				{
					if(current_heuristical_value < lowest) //if lower, reset the sequence counter, 
					{
						sequence_counter = 0;
						lowest = current_heuristical_value;
						best_candidates[sequence_counter][0][ROW] = possible_moves_array[i][ROW];
						best_candidates[sequence_counter][0][COLUMN] = possible_moves_array[i][COLUMN];
						++sequence_counter;
					}
					else if(current_heuristical_value == lowest)
					{
						best_candidates[sequence_counter][0][ROW] = possible_moves_array[i][ROW];
						best_candidates[sequence_counter][0][COLUMN] = possible_moves_array[i][COLUMN];
						++sequence_counter;
					}
				}
			}
			//Pick a move and do it
			pick = rand() % sequence_counter;
			for (int i = 0; i < 2; ++i)
			{
				move_sequence[move_counter][ROW] = best_candidates[pick][i][ROW];
				move_sequence[move_counter++][COLUMN] = best_candidates[pick][i][COLUMN];
				player_reposition(player_pos, best_candidates[pick][i]);
				readjust_heuristics_slow(); //For displaying purposes
				print_board_and_heuristics();
			}
			readjust_heuristics_slow(board, board_heuristics);
		}
		else if(resulting_move_count == 0) //No possible moves
		{
			lost = true;
		}
		else //If there is a move with a calculated lowest heuristical value
		{ 
			move_sequence[move_counter][ROW] = resulting_moves[0][ROW];
			move_sequence[move_counter++][COLUMN] = resulting_moves[0][COLUMN];
			player_reposition(player_pos, resulting_moves[0]);
			readjust_heuristics_slow(); //For displaying purposes
			print_board_and_heuristics();
		}
		if(check_lost(possible_moves_count))
		{
			print_board_and_heuristics();
			P("YOU LOST!");
			print_previous_moves(move_sequence, move_counter);
			exit(EXIT_SUCCESS);
			break;
		}
		if(check_win(player_pos))
		{
			print_board_and_heuristics();
			P("YOU WIN!");
			print_previous_moves(move_sequence, move_counter);
			exit(EXIT_SUCCESS);
		}
	}
}

//Returns moves with lowest heuristics, however, if moves have a tie, it will return it both
//Puts it in returned_moves array, pass a empty one to that argument
void get_move_with_lowest_heuristics(const int possible_moves[MOVE_LIMIT][VECTOR], const int possible_move_count, int returned_moves[MOVE_LIMIT][VECTOR], int * returned_move_count)
{
	int lowest = 9;
	*returned_move_count = 0;
	for (int i = 0; i < possible_move_count; ++i)
	{
		if(board_heuristics[possible_moves[i][ROW]][possible_moves[i][COLUMN]] < lowest)
		{
			*returned_move_count = 0;
			lowest = board_heuristics[possible_moves[i][ROW]][possible_moves[i][COLUMN]];
			returned_moves[*returned_move_count][ROW] = possible_moves[i][ROW];
			returned_moves[*returned_move_count][COLUMN] = possible_moves[i][COLUMN];
			*returned_move_count+=1;
		}
		else if(board_heuristics[possible_moves[i][ROW]][possible_moves[i][COLUMN]] == lowest)
		{
			returned_moves[*returned_move_count][ROW] = possible_moves[i][ROW];
			returned_moves[*returned_move_count][COLUMN] = possible_moves[i][COLUMN];
			*returned_move_count+=1;
		}
	}
}

bool check_win(const int player_pos[VECTOR]){
	for (int row = 0; row < ROWS; ++row)
	{
		for (int column = 0; column < COLUMNS; ++column)
		{
			if(board[row][column] != 'P' && board[row][column] != LANDED)
			{
				return false;
			}
		}
	}
	return true;
}

bool check_lost(const int possible_moves_count)
{
	return possible_moves_count <= 0;
}

void prompt(void)
{
	int x = 0, y = 0;
	P("Welcome to Knight's Tour!");
	P("Can a knight alone can traverse a chessboard where all of the cells are only touched once?");
	P("This is a self-solving (hopefully) knights tour.");
	
	while(true){
		printf("%s\n", "Enter 2 numbers less than 8 as your initial position.");
		//If has 2 inputs and both inputs is less than the specified rows and columns, by default is 8
		if(scanf("%d %d", &x, &y) == 2 && ((x > 0 && x < ROWS) && (y > 0 && y < COLUMNS))){
			INITIAL_POSITION[0] = x;
			INITIAL_POSITION[1] = y;
			fflush(stdin);
			return;
		}
		else
		{
			P("Error in input, repeating");
			fflush(stdin);
		}
	}
}

//No invalidated positions check
void moves_get(const int player_pos[VECTOR], int moves[MOVE_LIMIT][VECTOR], int *possible_moves_number)
{
	* possible_moves_number = 0;
	for (int i = 0, row = -1, column = -1; i < MOVE_LIMIT; ++i)
	{
		//The book got it mixed so...
		row = MOVE_MATRIX[i][COLUMN] + player_pos[ROW];
		column = MOVE_MATRIX[i][ROW] + player_pos[COLUMN];

		if(DEBUG)
				printf("Processing: Pos {%d, %d} Move %d to {%d, %d}\n", player_pos[ROW], player_pos[COLUMN], *possible_moves_number, row, column);

		//If row is in bounds or has not stepped on the cell yet
		if((row >= 0 && row < ROWS) && (column >= 0 && column < COLUMNS)
			&& board[row][column] != LANDED)
		{
			moves[*possible_moves_number][ROW] = row;
			moves[*possible_moves_number][COLUMN] = column;
			*possible_moves_number+= 1;
		}
	}

	if(DEBUG)
			printf("Position {%d, %d}: Possible Moves = %d\n", player_pos[ROW], player_pos[COLUMN], *possible_moves_number);

	if(DEBUG)
		for (int i = 0; i <= *possible_moves_number; ++i)
			printf("Position {%d, %d} Move %d: {%d, %d}\n", player_pos[ROW], player_pos[COLUMN], i, moves[i][ROW], moves[i][COLUMN]);

	//Fill the others with placeholder sentinel values
	for(int i = *possible_moves_number; i < MOVE_LIMIT; i++){
		moves[i][ROW] = -1;
		moves[i][COLUMN] = -1;
	}
}

//With invalidated moves, for branching applications
void moves_get_w_invalidated(const int player_pos[VECTOR], int moves[MOVE_LIMIT][VECTOR], int *possible_moves_number, const int invalidated[MEMORY_LIMIT][VECTOR], const int invalidated_move_count)
{
	* possible_moves_number = 0;
	for (int i = 0, row = -1, column = -1; i < MOVE_LIMIT; ++i)
	{
		//The book got it mixed so...
		row = MOVE_MATRIX[i][COLUMN] + player_pos[ROW];
		column = MOVE_MATRIX[i][ROW] + player_pos[COLUMN];

		//If row is in bounds or has not stepped on the cell yet
		if((row >= 0 && row < ROWS) && (column >= 0 && column < COLUMNS)
			&& board[row][column] != LANDED &&
			valid(invalidated, row, column))
		{
			moves[*possible_moves_number][ROW] = row;
			moves[*possible_moves_number][COLUMN] = column;
			*possible_moves_number+= 1;
		}
	}

	//Fill the others with placeholder sentinel values
	for(int i = *possible_moves_number; i < MOVE_LIMIT; i++){
		moves[i][ROW] = -1;
		moves[i][COLUMN] = -1;
	}
}

void board_clean()
{
	for(int row = 0; row < ROWS; ++row)
	{
		for (int column = 0; column < COLUMNS; ++column)
		{
			//If not has been there, or if not currently positioned there
			if(board[row][column] != LANDED && board[row][column] != 'P')
			{
				board[row][column] = ' ';
			}
		}
	}
}

void board_update(const int player_pos[VECTOR], const int possible_moves_array[MOVE_LIMIT][VECTOR], const int possible_moves_number)
{
	board[player_pos[ROW]][player_pos[COLUMN]] = 'P';
	for(int i = 0; i < possible_moves_number; ++i)
	{
		board[possible_moves_array[i][ROW]][possible_moves_array[i][COLUMN]] = '0' + i;
	}
}

void player_reposition(int current_position[VECTOR], const int new_position[VECTOR]){
	//put a bang in old position
	board[current_position[ROW]][current_position[COLUMN]] = LANDED;
	//put a P in new position
	board[new_position[ROW]][new_position[COLUMN]] = 'P';
	current_position[ROW] = new_position[ROW];
	current_position[COLUMN] = new_position[COLUMN];
}

//Only prints the board and necessary 
void board_print()
{
	puts("--- Game Board ---");
	for (int row = 0; row < ROWS; ++row)
	{
		for(int column = 0; column < COLUMNS; ++column)
		{
			cell_print(board[row][column]);
		}
		puts("");
	}
}

void cell_print(char content)
{
	printf("|%c|", content);
}

bool valid(const int invalid_moves[MEMORY_LIMIT][VECTOR], const int row, const int column)
{
	for (int i = 0; i < MEMORY_LIMIT; ++i)
	{
		if(invalid_moves[i][ROW] == row && invalid_moves[i][COLUMN] == column)
		{
			return false;
		}
	}
	return true;
}

//Gets heuristical value based on how many moves it can make on that position
void readjust_heuristics_slow()
{
	int possible_moves = 0;
	int possible_moves_placeholder[MOVE_LIMIT][VECTOR];
	int pos_placeholder[VECTOR] = {-1,-1};
	for (int row = 0; row < ROWS; ++row)
	{
		for (int column = 0, possible_moves = 0; column < COLUMNS; ++column)
		{
			//Get amount heuristic amount on a specific position, that is, not yet been touched
			pos_placeholder[ROW] = row;
			pos_placeholder[COLUMN] = column;
			moves_get(pos_placeholder, possible_moves_placeholder, &possible_moves);
			board_heuristics[row][column] = possible_moves;
		}
	}
}

void print_board_and_heuristics()
{
	//puts("--- Game Board --- Heuritics ---");
	printf("%24s%4s%-24s\n", "   ---Game Board---    ", "----", "    ---Heuristics---   ");
	for (int row = 0; row < ROWS; ++row)
	{
		for(int column = 0; column < COLUMNS; ++column)
		{
			cell_print(board[row][column]);
		}
		printf("%4s", "");
		for(int column = 0; column < COLUMNS; ++column)
		{
			cell_print(board_heuristics[row][column] + '0');
		}
		puts("");
	}
}

void print_previous_moves(const int move_sequence[MEMORY_LIMIT][VECTOR], const int move_count)
{
	for (int i = 0; i < move_count; ++i)
	{
		printf("Move %d: {%d, %d}\n", i + 1, move_sequence[i][ROW] + 1, move_sequence[i][COLUMN] + 1);
	}
}