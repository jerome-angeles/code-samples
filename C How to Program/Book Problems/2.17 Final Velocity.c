/*
Name: Final Velocity
Author: Jerome Angeles
Date: 7/2/18

(Final Velocity) Write a program than asks the user to enter the initial velocity and acceleration of an object, and the time that has elapsed, places them in the variables u (initial velocity), a, and t, and prints the final velocity, v, and distance traversed, s, using the following equations.
a)v = u + at
b)s = ut + (1/2)at^2

No pointers and CLI arguments (yet), no ifs
*/

#include <stdio.h>
#include <math.h>

int main(void) {
	double v = 0.0,s = 0.0,vi = 0.0,a = 0.0,t = 0.0;

	printf("C: How to Program\nProblem 2.17 - Final Velocity\n\n");
	printf("%s", "Enter initial velocity: ");
	scanf("%lf", &vi); //lf for LONG FLOAT (or a double)
	printf("%s", "Enter acceleration: ");
	scanf("%lf", &a);
	printf("%s", "Enter time: ");
	scanf("%lf", &t);

	v = vi + a * t;
	s = vi * t + pow(0.5*a*t, 2);

	printf("Final Velocity: %.2f\n", v);
	printf("Distance Traversed: %.2f\n", s);
	return 0;
}