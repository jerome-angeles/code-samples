/*
Name: Knight's Tour
Author: Jerome Angeles
Date: 7/6/2018 - 7/7/2018

Full pledged knight's tour (not self-solving yet)
*/

#include "6.24 Knight's Tour.h"

int main(void)
{
	//Initialize the board
	for (int row = 0; row < ROWS; ++row)
	{
		for(int column = 0; column < COLUMNS; ++column)
		{
			board[row][column] = ' ';
		}
	}
	prompt();
	int player_pos[VECTOR];
	player_pos[ROW] = INITIAL_POSITION[ROW] - 1;
	player_pos[COLUMN] = INITIAL_POSITION[COLUMN] - 1;
	loop(player_pos);
	exit(EXIT_FAILURE);
}

void loop(int player_pos[VECTOR])
{
	int possible_moves_array[MOVE_LIMIT][VECTOR];
	int possible_moves_count = -1;
	int player_input = -1;
	bool lost = false, won = false;
	while(!lost || !won)
	{
		player_input = -1;
		//calculate possible moves
		moves_get(player_pos, possible_moves_array, &possible_moves_count);
		//check for loss condition
		if(lost = check_lost(possible_moves_count))
		{
			board_print(board);
			break;
		}
		//edit board as neccesary, show buttons to press
		board_update(player_pos, possible_moves_array, possible_moves_count);
		//print board
		board_print();
		//ask input
		player_input = ask_input(possible_moves_count);
		//reposition player and put a bang on the last position
		player_reposition(player_pos, possible_moves_array[player_input]);
		//clean board
		board_clean();
		//Check for win condition
		won = check_win(player_pos);
	}

	if(won)
	{
		P("YOU WIN!");
		exit(EXIT_SUCCESS);
	}
	if(lost)
	{
		P("YOU LOST!");
		exit(EXIT_SUCCESS);
	}
}

bool check_win(const int player_pos[VECTOR]){
	for (int row = 0; row < ROWS; ++row)
	{
		for (int column = 0; column < COLUMNS; ++column)
		{
			if(board[row][column] != 'P' && board[row][column] != LANDED)
			{
				return false;
			}
		}
	}
	return true;
}

bool check_lost(const int possible_moves_count){
	return possible_moves_count <= 0;
}

int ask_input(const int possible_moves_count)
{
	int input = -1;
	while(true)
	{
		printf("%s", "Enter next move: ");
		//if valid input
		if(scanf("%d", &input) && (input >= 0 && input < possible_moves_count))
		{
			fflush(stdin);
			return input;
		}
		else
		{
			P("Wrong input: Try again!");
			fflush(stdin);
		}
	}
}

void prompt(void)
{
	int x = 0, y = 0;
	P("Welcome to Knight's Tour!");
	P("Can a knight alone can traverse a chessboard where all of the cells are only touched once?");
	
	while(true){
		printf("%s\n", "Enter 2 numbers less than 8 as your initial position.");
		//If has 2 inputs and both inputs is less than the specified rows and columns, by default is 8
		if(scanf("%d %d", &x, &y) == 2 && ((x > 0 && x <= ROWS) && (y > 0 && y <= COLUMNS))){
			INITIAL_POSITION[0] = x;
			INITIAL_POSITION[1] = y;
			fflush(stdin);
			return;
		}
		else
		{
			P("Error in input, repeating");
			fflush(stdin);
		}
	}
}

void moves_get(const int player_pos[VECTOR], int moves[MOVE_LIMIT][VECTOR], int *possible_moves_number)
{
	* possible_moves_number = 0;
	for (int i = 0, row = -1, column = -1; i < MOVE_LIMIT; ++i)
	{
		//The book got it mixed so...
		row = MOVE_MATRIX[i][COLUMN] + player_pos[ROW];
		column = MOVE_MATRIX[i][ROW] + player_pos[COLUMN];

		//If row is in bounds or has not stepped on the cell yet
		if((row >= 0 && row < ROWS) && (column >= 0 && column < COLUMNS)
			&& board[row][column] != LANDED)
		{
			moves[*possible_moves_number][ROW] = row;
			moves[*possible_moves_number][COLUMN] = column;
			*possible_moves_number+= 1;
		}
	}
	printf("moves number %d\n", *possible_moves_number);

	//Fill the others with placeholder sentinel values
	for(int i = *possible_moves_number; i < MOVE_LIMIT; i++){
		moves[i][ROW] = -1;
		moves[i][COLUMN] = -1;
	}
}

void board_clean()
{
	for(int row = 0; row < ROWS; ++row)
	{
		for (int column = 0; column < COLUMNS; ++column)
		{
			//If not has been there, or if not currently positioned there
			if(board[row][column] != LANDED && board[row][column] != 'P')
			{
				board[row][column] = ' ';
			}
		}
	}
}

void board_update(const int player_pos[VECTOR], const int possible_moves_array[MOVE_LIMIT][VECTOR], const int possible_moves_number)
{
	board[player_pos[ROW]][player_pos[COLUMN]] = 'P';
	for(int i = 0; i < possible_moves_number; ++i)
	{
		board[possible_moves_array[i][ROW]][possible_moves_array[i][COLUMN]] = '0' + i;
	}
}

void player_reposition(int current_position[VECTOR], const int new_position[VECTOR]){
	//put a bang in old position
	board[current_position[ROW]][current_position[COLUMN]] = LANDED;
	//put a P in new position
	board[new_position[ROW]][new_position[COLUMN]] = 'P';
	current_position[ROW] = new_position[ROW];
	current_position[COLUMN] = new_position[COLUMN];
}

//Only prints the board
void board_print()
{
	puts("--- Game Board ---");
	for (int row = 0; row < ROWS; ++row)
	{
		for(int column = 0; column < COLUMNS; ++column)
		{
			cell_print(board[row][column]);
		}
		puts("");
	}
}

void cell_print(char content)
{
	printf("|%c|", content);
}