/*
Name: World Population Calculator
Author: Jerome Angeles
Date: 7/3/18

Use the web to determine the current world pop-ulation and the annual world population growth rate. Write an application that inputs these values,then displays the estimated world population after one, two, three, four and five years. 

No pointers and CLI arguments (yet).
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define WORLD_POPULATION 7632819325
#define GROWTH_RATE 0.012

int main(void)
{
	double world_pop = WORLD_POPULATION;
	double new_world_pop = 0.0;
	int years = 0;

	printf("Current World Population: %.0lf\n Enter years in advance: ", world_pop);
	if(!scanf("%d", &years)){
		printf("%s\n", "Wrong input desuyo");
		exit(EXIT_FAILURE);
	}
	else{
		new_world_pop = world_pop * pow(1.0 + GROWTH_RATE, years);
		printf("\nWorld Population in %d years is %.0lf\n", years, new_world_pop);
		exit(EXIT_SUCCESS);
	}
}