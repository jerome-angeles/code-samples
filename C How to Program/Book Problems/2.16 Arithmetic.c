/*
Name: Arithmetic
Author: Jerome Angeles
Date: 7/2/18

Write a program that asks the user to enter two numbers, obtains them from the user and prints their sum, product, difference, quotient and remainder.

No pointers and CLI arguments (yet), no ifs
*/

#include <stdio.h>

int main(void) {
	int num1 = 0;
	int num2 = 0;

	printf("%s", "Enter 2 numbers: ");
	scanf("%d%d", &num1, &num2);

	printf("%-10s%8d\n", "Sum", num1+num2);
	printf("%-10s%8d\n", "Difference", num1-num2);
	printf("%-10s%8d\n", "Product", num1*num2);
	printf("%-10s%8d\n", "Quiotient", num1/num2);
	printf("%-10s%8d\n", "Remainder", num1%num2);
	
	return 0;
}