//EOF Sample
//Grade entering system using swithc

#include <stdio.h>

int main(void){

	int aGrades, bGrades, cGrades, dGrades, eGrades;
	int input;

	//Initialize numbers to 0 to avoid errors
	aGrades = bGrades = cGrades = dGrades = eGrades = 0;

	printf("%s", "Enter Grades: ");

	while((input = getchar()) != EOF || input == '\n'){
		switch (input){
			case 'a':
			case 'A':
				aGrades += 1;
				break;
			case 'b':
			case 'B':
				bGrades += 1;
				break;
			case 'c':
			case 'C':
				cGrades += 1;
				break;
			case 'd':
			case 'D':
				dGrades += 1;
				break;
			case 'e':
			case 'E':
				eGrades += 1;
				break;
		}
	}
	printf("%10s %5s\n", "Grade", "Count");
	printf("%10s %5d\n", "A", aGrades);
	printf("%10s %5d\n", "B", bGrades);
	printf("%10s %5d\n", "C", cGrades);
	printf("%10s %5d\n", "D", dGrades);
	printf("%10s %5d\n", "E", eGrades);
	return 0;
}