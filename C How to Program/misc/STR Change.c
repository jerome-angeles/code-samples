/*
Name: STR Change
Author: Jerome Angeles
Date: 7/6/2018
*/

#include <stdio.h>

int main(void)
{
	char str[] = {'H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd', '!', '\0'};
	char in = ' ';
	printf("%s\n", "Can you change a letah? Enter a character");
	if(scanf("%c", &in)){
		str[12] = in;
	}
	else{
		printf("%s\n", "Got it wrong bruh");
		return 1;
	}
	printf("%s\n", str);
	return 0;
}