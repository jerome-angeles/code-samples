//Lucky 9 game
//Without using pointers
//With AI enemy (FSM)
//AI Behavior
// Draws cards -> if hand <= 5 has a 60% chance to draw another card
//						  <= 2 has a 100% chance to draw another card

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define EMPTY_HAND {0,0,0}

//Function prototypes
//Draws a card from 1 (ACE) to 10, actually just returns a number from 1 - 10
int draw(void);
//Calculate score of a card
int calculate_score(int cards[]);
//Defines a standard way to print a card
void print_cards(int cards[]);
//Gets the first character of a string input
char getfc(void);
//AI functions
//Check if AI should draw another
bool ai_draw_another(int ai_score);

int main(){

	char input;
	int game_score_player = 0;
	int game_score_ai = 0;

	//Set seed
	srand(time(NULL));

	puts("Lucky 9");
	getfc();

	puts("Playing Lucky 9, press q to end game");
	while(input != 'q' || input != 'Q'){
		//cards
		int p_cards[] = EMPTY_HAND;
		int ai_cards[] = EMPTY_HAND;
		int hand_score_player = 0;
		int hand_score_ai = 0;
		//Player draws 2 cards
		p_cards[0] = draw();
		p_cards[1] = draw();
		print_cards(p_cards);

		//AI draws 2 cards, does not print it
		ai_cards[0] = draw();
		ai_cards[1] = draw();
		hand_score_ai = calculate_score(ai_cards);
		if(ai_draw_another(hand_score_ai)){
			ai_cards[2] = draw();
			//Makes a second calculation
			hand_score_ai = calculate_score(ai_cards);
		}
		
		//Asks to draw another?
		printf("%s", "Draw another? (y/n)");
		input = getfc();
		if(input == 'y' || input == 'Y'){
			p_cards[2] = draw();
			print_cards(p_cards);
		}
		hand_score_player = calculate_score(p_cards);

		printf("%s", "AI Cards: ");
		print_cards(ai_cards);

		printf("%-10s%d\n", "Score:", hand_score_player);

		if(hand_score_player == hand_score_ai){
			printf("DRAW! AI: %d PLAYER: %d\n", hand_score_ai, hand_score_player);
		}
		else if(hand_score_player <= hand_score_ai){
			game_score_ai += 1;
			printf("AI WIN! AI: %d PLAYER: %d\n", hand_score_ai, hand_score_player);
		}
		else{
			game_score_player += 1;
			printf("PLAYER WIN! AI: %d PLAYER: %d\n", hand_score_ai, hand_score_player);	
		}

		//play again?
		printf("%s", "Play Again?");
		input = getfc();
	}

	return EXIT_SUCCESS;
}

int draw(void){
	return 1 + rand() % 10;
}

int calculate_score(int cards[]){
	return (cards[0] + cards[1] + cards[2]) % 10;
}

void print_cards(int cards[]){
	cards[2] == 0 ? printf("Cards: %d, %d\n", cards[0], cards[1]) : printf("Cards: %d, %d, %d\n", cards[0], cards[1], cards[2]);
}

char getfc(){
	char input[255];
	if(scanf("%s", &input)){
		return input[0];
	}
	else{
		return -1;
	}
}

bool ai_draw_another(int ai_score){
	if(ai_score <= 3)
		return true;
	else if (ai_score <= 5 && rand() % 100 < 60)
		return true
	else
		return false;
}