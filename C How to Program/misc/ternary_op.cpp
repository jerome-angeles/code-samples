/*
Grade program with ternary operator
*/

#include <stdio.h>

int main(void)
{
	float grade;

	printf("%s", "Enter student grade: ");
	scanf("%f", &grade);

	puts(grade < 75 ? "Student failed" : "Student passed");
	return 0;
}