/*
Name: String Input
Author: Jerome Angeles
Date: 7/6/2018
*/

#include <stdio.h>

int main(void)
{
	char str[50];

	printf("%s", "Input a string with a space: ");
	scanf("%s", str);
	printf("Input is %s", str);
	return 0;
}