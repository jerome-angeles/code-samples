/*
Grading Program

The classic one
*/

#include <stdio.h>

int main(void){
	float grade;

	printf("%s", "Enter student grade: ");
	scanf("%f", &grade);

	if(grade < 75.0f){
		puts("Your student failed");
	}
	else{
		puts("He passed by the way");
	}

	return 0;

}