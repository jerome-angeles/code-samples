//Compound interest calculator
#include <stdio.h>
#include <math.h>

int main(void){
	double principal = 1000.0;
	double rate = 0.05;

	printf("%4s %21s\n", "Year", "Amount on Deposit");

	for(int year = 1; year <= 100; year++){
		printf("%4u %21.2f\n", year, principal * pow(1.0+rate, year));
	}

	return 0;
}