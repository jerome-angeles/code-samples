//Function sample
//Square function

#include <stdio.h>

int square(int x); //function prototype

int main(void){
	int input = 0;
	int output = 0;

	printf("%s", "Enter number ");

	//Checks if input is indeed a number and is greater than 1
	if (scanf("%d", &input) && input >= 1){
		output = square(input);
		printf("The square of %d is %d", input, output);
		return 0;
	}
	else{
		puts("Invalid Input, rerun the program");
		return 1;
	}
}

//Squares the number "x"
int square(int x){
	return x * x;
}