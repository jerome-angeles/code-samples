//Add 2 numbers

#include <stdio.h>

int main(void){
	int number1;
	int number2;

	printf("Enter the first number: ");
	scanf("%d", &number1);

	printf("Enter the second number: ");
	scanf("%d", &number2);

	printf("The sum of %d and %d is %d", number1, number2, number1+number2);
	return 0;

}