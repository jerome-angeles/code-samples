#include "chip8.h"

void chip8_init(){

}

void chip8_loop(){
    //Handle delay timer and sound timer.
    if(reg_dt > 0){
        reg_dt--;
    }
    if(reg_st > 0){
        reg_st--;
    }
}

void clock_tick(void * callBack()){
    nanosleep(&sleepspec, NULL);
    callBack();
}

int main(int argc, char ** argv){
    chip8_init();
    //TODO: Find out how to exit chip8
    while(1){
        clock_tick(&chip8_loop);
    }
    return 0;
}