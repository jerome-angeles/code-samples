#ifndef chip8
    #define chip8
    #define CHIP8_MEMORY_SIZE 0xFFF
    #define CHIP8_GENERAL_PURPOSE_REGISTER_COUNT 0xF
    #define CHIP8_STACK_MAX_COUNT 0xF
    #define CHIP8_SOUND_FREQ 2093 //C7
	#define CHIP8_SCREEN_WIDTH 32
	#define CHIP8_SCREEN_HEIGHT 64

    #include <stdlib.h>
    #include <pthread.h>
    #include <time.h>
    
    
    unsigned char mem[CHIP8_MEMORY_SIZE];
    unsigned char graphics[CHIP8_SCREEN_WIDTH][CHIP8_SCREEN_HEIGHT];
    //TODO: Put on the characters onto the memory http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
    unsigned char reg_gp[CHIP8_GENERAL_PURPOSE_REGISTER_COUNT];
    unsigned short reg_I;
    //TODO: Know how big this VF register should be
    unsigned short reg_VF; 
    unsigned char reg_dt; //delay timer
    unsigned char reg_st; //sound timer
    unsigned short reg_pc; //program counter
    unsigned char reg_sp; //stack pointer
    unsigned short reg_stack[CHIP8_STACK_MAX_COUNT];
    struct timespec sleepspec = {.tv_nsec = 16666666L};

    void chip8_init();
    void chip8_loop();
    //* Is implemented using pthreads in order for the delay to be seperate from the main clock
    //* Documentation says that delay should decrement by 1 60hz
    void clock_tick(timespec, void * ());

#endif