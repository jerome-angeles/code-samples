# Code Samples

Jerome Angeles

Mini-projects, activities, MOOC Assignments, all to be dumped here for anyone who have it's link to see. Will be used for showcasing purposes.

Official projects have their own links also provided below, projects from the companies I worked with are also here.

## Official Projects

* [Bachelor's Thesis - The Heir](https://mega.nz/#!RrplDSqa!E9OJFhXM6T7bIURzr2FAKwDAZsyFI6vsi4GLtuhAXF4)(Unity)
* [Rational Expressions CAI](https://bitbucket.org/raizumaraiz/rational-expressions-cai/)(Visual Studio)
* [Help Arki](https://bitbucket.org/wiredreams/help-arki)(Unity)
* Biometric Login System - With Rotek Trading Corporation
    * Employees can now login to the Rotek ERP using their fingerprints that act like passwords.
* ERP Modules for inhouse ERP to export data for Unionbank's Warehousing and Checkwriting.

## Sections

* C How to Program - A introductory book into C that contains lots of activities of various difficulty. Some activities I think is hard enough to be a small project, like "6.24 Knight's Tour"
* Learn Python the Hard Way - An introductory book to Python (for total beginners) composed of mostly easy activities.
* [Python Data Analysis](https://www.coursera.org/learn/python-data-analysis/home/welcome) - A MOOC (Massively Open Online Course) about Data Analysis offered in Coursera by University of Michigan. Needs Python 3.6 and Pandas library, some assignments are written using Jupyter Notebook and thus needs it to be run. Install Anaconda Navigator to get all the prerequisites.
* [rDailyProgrammer](https://www.reddit.com/r/dailyprogrammer/) - A reddit page where coding challenges are hosted. Programming language used to complete the challenge might be completely random based on what I am learning at the time of completion

## Prerequisites

If you are trying to try out some of the code you need to have the following

### C

* gcc compiler

### Python

* Python 3.6
* For Data Analysis
    * Pandas Library
    * Anaconda Navigator

### NodeJS

* The latest version of node
* npm, please install all modules before running script.

This is not all of my code that I made througout the years, if I find some code samples in the depths of my hard drives as well as my school projects and other personal projects I will add them here as I see fit.

Please do not share unless needed.
